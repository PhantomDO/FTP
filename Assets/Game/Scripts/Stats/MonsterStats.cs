﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New MonsterStats", menuName = ("MonsterStas"))]
public class MonsterStats : ScriptableObject {

    public float _maxHealth;
    public float _damage;
    public float _lvl;
}
