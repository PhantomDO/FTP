﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New EnemyStats", menuName = ("EnemyStats"))]
public class EnemyStats : ScriptableObject
{

    public float _maxHealth;
    public float _damage;
}
