﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New BuildingStats", menuName = ("BuildingStats"))]
public class BuildingStats : ScriptableObject
{
    public float _maxHealth;
    public float _radius;
    public int _gold;
    public int _diamond;
}
