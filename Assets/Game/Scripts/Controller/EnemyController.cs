﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyHealth))]
public class EnemyController : EnemyHealth {

    #region Variables

    [Header("Parameters")]
    public float radius;
    public LayerMask searchForPlayer;

    private GameObject target;
    #endregion

    #region Unity Methods

    protected override void Start()
    {
        //GET STATS 
    }

    void Update()
    {
        Checking();
    }
    #endregion

    #region Check

    private void Checking()
    {

        Collider[] fov = Physics.OverlapSphere(transform.position, radius, searchForPlayer);

        for (int i = 0; i < fov.Length; i++)
        {
            target = fov[i].gameObject;
            Vector3 direction = target.transform.position - transform.position;
            RaycastHit hit;

            if (Physics.Raycast(transform.position, direction, out hit, searchForPlayer))
            {
                Debug.Log("AN ENENMY AS BEEN SPOTTED !");
            }
        }
    }
    #endregion

}
