﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingController : BuildingHealth {

    #region Variables

    [Header("References")]
    public BuildingStats stat;

    [Header("Parameters")]
    public float radius;
    public LayerMask searchForPlayer;

    [HideInInspector] public int gold;
    [HideInInspector] public int diamond;

    private GameObject target;

    public bool enemyHere;
    #endregion

    #region Unity Methods

    protected override void Awake()
    {
        //GET STATS
        if (stat != null)
        {
            maxHealth = stat._maxHealth;
            base.Awake();

            radius = stat._radius;
            gold = stat._gold;
            diamond = stat._diamond;
        }
    }

    void Update()
    {
        Checking();

        //Debug.Log("currentHealth Batiment : " + currentHealth);
    }
    #endregion

    #region Check

    private void Checking()
    {
        enemyHere = false;

        Collider[] fov = Physics.OverlapSphere(transform.position, radius, searchForPlayer);

        for (int i = 0; i < fov.Length; i++)
        {
            target = fov[i].gameObject;
            Vector3 direction = target.transform.position - transform.position;
            RaycastHit hit;

            //float distance = Vector3.Distance(transform.position, target.transform.position);

            if (Physics.Raycast(transform.position, direction, out hit, searchForPlayer))
            {
                //Debug.Log("Enemy here !");
                enemyHere = true;
            }
        }
    }
    #endregion
}
