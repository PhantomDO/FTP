﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MonsterController : MonsterHealth
{

    #region Variables

    public enum StatePlayer { IDLE, WALK, ATTACK }
    public StatePlayer state;

    [Header("References")]
    public MonsterStats stat;
    private LineRenderer lr;
    private Rigidbody rB;

    [Header("Searching")]
    public LayerMask searchForBuilding;
    public float radius;

    [HideInInspector] public List<GameObject> visibleTarget = new List<GameObject>();
    private GameObject target;

    private float distance;
    [HideInInspector] public float damage;
    [HideInInspector] public float lvl;

    private bool targetConfirmed;

    private float nextShoot;
    public float shootRate;
    private WaitForSeconds shootDuration = new WaitForSeconds(0.07f);

    RaycastHit hit;

    public Animator animator;

    [Header("MOVE WITHOUT NAVMESH AGENT")]
    public float speed;
    Vector3 localPosAtInstant;
    Vector3 straightForward;
    bool activePath;

    #endregion

    #region Unity Methods
    protected override void Start()
    {
        //GET STATS
        if (stat != null)
        {
            maxHealth = stat._maxHealth;
            damage = stat._damage;
            lvl = stat._lvl;
        }

        base.Start();

        lr = GetComponent<LineRenderer>();
        lr.enabled = false;
        rB = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Search();
        State();

        Debug.Log("LVL MONSTER : " + lvl + ", " + "Index : " + GetComponentInParent<MonsterManager>()._monster.IndexOf(this.gameObject));
    }
    #endregion

    #region Searching
    private void Search()
    {
        visibleTarget.Clear();
        targetConfirmed = false;

        Collider[] fov = Physics.OverlapSphere(transform.position, radius, searchForBuilding);

        for (int i = 0; i < fov.Length; i++)
        {
            target = fov[i].gameObject;
            Vector3 direction = target.transform.position - transform.position;

            distance = Vector3.Distance(transform.position, target.transform.position);

            if (Physics.Raycast(transform.position, direction, out hit, distance, searchForBuilding))
            {
                visibleTarget.Add(target);
                targetConfirmed = true;
                Debug.DrawLine(transform.position, target.transform.position);
            }
        }
    }
    #endregion

    #region Movement
    private void State()
    {
        if (distance <= radius && targetConfirmed == true)
        {
            state = StatePlayer.ATTACK;
        }
        else
        {
            state = StatePlayer.IDLE;
        }

        switch (state)
        {
            case StatePlayer.IDLE:
                //animator.SetBool("Running", true);
                Idle();
                break;
            case StatePlayer.WALK:
                Walk();
                break;
            case StatePlayer.ATTACK:
                //animator.SetBool("Running", false);
                Attack();
                break;
            default:
                Idle();
                break;
        }
    }

    private void Idle()
    {
        activePath = true;

        if (activePath)
        {
            localPosAtInstant = transform.position;
            activePath = false;
        }

        straightForward = localPosAtInstant + rB.transform.forward * speed;
        transform.position = Vector3.MoveTowards(transform.position, straightForward, speed * Time.deltaTime);
        state = StatePlayer.WALK;
    }

    private void Walk()
    {
        if (Vector3.Distance(transform.position, straightForward) <= radius && !activePath)
        {
            state = StatePlayer.IDLE;
        }
    }
    #endregion

    #region Attack
    private void Attack()
    {
        if (Time.time > nextShoot)
        {
            nextShoot = Time.time + shootRate;

            StartCoroutine(ShootEffect());

            lr.SetPosition(0, transform.position);
            lr.SetPosition(1, target.transform.position);

            if (hit.collider != null)
            {
                ApplyDamage();
            }
        }
    }

    private IEnumerator ShootEffect()
    {
        lr.enabled = true;

        string[] parameters = { "Left Punch Attack", "Right Punch Attack" };

        //animator.SetTrigger(parameters.RandomItem());

        yield return shootDuration;

        lr.enabled = false;
    }

    private void ApplyDamage()
    {
        IDamageable buildingStanding = hit.collider.GetComponentInParent<IDamageable>();
        if (buildingStanding != null)
        {
            buildingStanding.TakeHit(damage);
        }
    }
    #endregion
}

public static class ArrayExtensions
{
    public static T RandomItem<T>(this T[] array)
    {
        return array[Random.Range(0, array.Length)];
    }
}
