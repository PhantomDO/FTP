﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

    public Transform target;

    private void LateUpdate()
    {
        Vector3 distance = new Vector3(-50,50,0);
        transform.position = Vector3.Slerp(transform.position, target.position + distance, Time.deltaTime);
    }
}
