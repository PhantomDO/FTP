﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class ViewRadius : MonoBehaviour {

    #region Variables
    public float radius;
    #endregion

    #region Unity Methods

    private void Awake()
    {
        if(GetComponent<MonsterController>() != null)
            radius = GetComponent<MonsterController>().radius;
        if (GetComponent<BuildingController>() != null)
            radius = GetComponent<BuildingController>().radius;
    }

    private void OnDrawGizmos()
    {
        if (GetComponent<MonsterController>() != null)
            Gizmos.color = Color.cyan;
        if (GetComponent<BuildingController>() != null)
            Gizmos.color = Color.red;

        Gizmos.DrawWireSphere(transform.position, radius);
    }

    #endregion
}
