﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyManager : MonoBehaviour {
    
    #region Variables

        [Header("Diamond")]
        public int numberOfDiamond;
        [Header("Gold")]
        public int numberOfGold;

    #endregion

    #region Earn Money
        public void EarnDiamond(int diamondDrop)
        {
            numberOfDiamond += diamondDrop;
        }

        public void EarnGold(int goldDrop)
        {
            numberOfGold += goldDrop;
        }
    #endregion

    #region Decrease Money

    public void DecreaseDiamond(int diamondSpend)
        {
            numberOfDiamond -= diamondSpend;
        }

        public void DecreaseGold(int goldSpend)
        {
            numberOfGold -= goldSpend;
        }

    #endregion
}
