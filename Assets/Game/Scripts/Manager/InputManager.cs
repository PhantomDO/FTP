﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    #region Variables

    [Header("Damage")]
    public float minDamage;
    public float maxDamage;
    private float damage;

    [Header("Percentage")]
    public int percentMaxValue;
    private int percentCritical;

    [Header("Click Count")]
    private float clickCount;
    private float numberOfClickSinceStart;
    private bool a;

    [Header("References")]
    public GameObject FX;
    private BuildingManager buildings;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        buildings = FindObjectOfType<BuildingManager>();
    }

    private void Update()
    {
        CheckClick();
        TapScreen();
        ClickMouse();
        CriticalPercent();
    }
    #endregion

    #region Check

    private void CheckClick()
    {
        a = true;
        if (Input.GetMouseButtonDown(0) && a)
        {
            clickCount++;
            numberOfClickSinceStart++;
            a = false;
        }
    }
    #endregion

    #region Click/Tap

    private void ClickMouse()
    {
        //MOUSE
        for (int mouseCount = 0; mouseCount < clickCount; ++mouseCount)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray))
            {
                if (buildings.GetComponentInChildren<BuildingController>().enemyHere == true)
                {
                    //DO DAMAGE
                    if (buildings != null)
                    {
                        IDamageable buildingStanding = buildings.GetComponentInChildren<IDamageable>();
                        if (buildingStanding != null)
                        {
                            buildingStanding.TakeHit(damage);
                            Instantiate(FX, ray.origin, Quaternion.identity);
                            percentCritical = Random.Range(1, 100);
                            clickCount = 0;
                        }
                    }
                }
            }
        }
    }

    private void TapScreen()
    {
        MonsterManager monsters = FindObjectOfType<MonsterManager>();

        for (int i = 0; i < Input.touchCount; ++i)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);

            if (Physics.Raycast(ray))
            {
                if (buildings.GetComponentInChildren<BuildingController>().enemyHere == true)
                {
                    //DO DAMAGE
                    if (buildings != null)
                    {
                        IDamageable buildingStanding = buildings.GetComponentInChildren<IDamageable>();
                        if (buildingStanding != null)
                        {
                            buildingStanding.TakeHit(damage);
                            Instantiate(FX, ray.origin, Quaternion.identity);
                            percentCritical = Random.Range(1, 100);
                        }
                    }
                }
            }
        }
    }
    #endregion

    #region Critic

    private void CriticalPercent()
    {
        if (percentCritical <= percentMaxValue)
        {
            damage = maxDamage;
        }
        else
        {
            damage = minDamage;
        }

        if (numberOfClickSinceStart >= 100)
        {
            damage = maxDamage;
        }
        else
        {
            damage = minDamage;
            numberOfClickSinceStart = 0;
        }
    }
    #endregion

}
