﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterManager : MonoBehaviour
{

    #region Variables

    [Header("Monster")]
    public List<GameObject> _monster = new List<GameObject>();
    private MonsterController[] monster;
    public GameObject monsterPrefab;
    public int numberOfMonster;
    public float DPStotal;

    [Header("Leveling")]

    [Header("Money To Upgrade")]
    public int gToBuyMonster;
    public int gToBuyUpgrade;
    [Header("Percent To Upgrade")]
    public float percentGBuy;
    public float percentGUp;
    public float percentUpLvl;
    [Header("Caracteristiques")]
    public float levelMax = 3;
    private float healthMonster;
    private float damageMonster;
    private float lvlMonster;

    private int index;

    private MoneyManager gM;

    [Header("Class Monster")]
    public List<GameObject> lineList = new List<GameObject>();
    private SeeMe[] lineArray;
    public GameObject linePrefab;
    public GameObject lineContainer;

    #endregion

    #region Unity Methods

    private void Start()
    {
        gM = FindObjectOfType<MoneyManager>();
        index = 1;
    }

    private void Update()
    {
        CheckNumberOfMonster();
        CheckNumberOfClassMonster();
        AssignMonsterToLine();

        Debug.Log("LVL UP : " + lvlMonster);
    }

    #endregion

    #region Checking Methods

    private void CheckNumberOfMonster()
    {
        monster = GetComponentsInChildren<MonsterController>();

        //CHECK LE NOMBRE DE MONSTRE
        for (int i = 0; i < monster.Length; i++)
        {
            if (!_monster.Contains(monster[i].gameObject))
            {
                _monster.Add(monster[i].gameObject);

                if (_monster.Count <= 1)
                {
                    CheckLevels(i);
                }

                numberOfMonster++;
            }

            IncreaseCaract(i);

            //Debug.Log("HPmax : " + monster[i].maxHealth + ", " + "Damage : " + monster[i].damage + ", " + "Level : " + monster[i].lvl + ". ");
        }
    }

    private void CheckNumberOfClassMonster()
    {
        int numberOfLine = 0;
        lineArray = lineContainer.GetComponentsInChildren<SeeMe>();
        //CHECK LE NOMBRE DE LIGNE DE MONSTRE
        for (int i = 0; i < lineArray.Length; i++)
        {
            if (!lineList.Contains(lineArray[i].gameObject))
            {
                lineList.Add(lineArray[i].gameObject);
                numberOfLine++;
            }
        }
    }

    private void CheckLevels(int first)
    {
        healthMonster = _monster[first].GetComponent<MonsterController>().maxHealth;
        damageMonster = _monster[first].GetComponent<MonsterController>().damage;
        lvlMonster = _monster[first].GetComponent<MonsterController>().lvl;
        DPStotal = (_monster[first].GetComponent<MonsterController>().damage * 4) * _monster.Count;
    }

    #endregion

    #region Increase Methods

    public void IncreaseLevel()
    {
        if (_monster[0].GetComponent<MonsterController>().lvl >= levelMax)
        {
            Debug.Log("YOU CAN'T");
        }
        else
        {
            IncreasePower();
            gM.DecreaseGold(gToBuyUpgrade);
            IncreaseMoney(gToBuyUpgrade, percentGUp);
        }
    }

    public void IncreaseMonster()
    {
        if (_monster.Count > 19)
        {
            Debug.Log("YOU CAN'T SPAWN ANYBODY");
        }
        else
        {
            //SPAWN MONSTER
            if (gM.numberOfGold >= gToBuyMonster)
            {
                InstantiateNewMonster();
                gM.DecreaseGold(gToBuyMonster);
                IncreaseMoney(gToBuyMonster, percentGBuy);
            }
        }
    }

    private void IncreasePower()
    {
        bool damnLvl = true;
        if (damnLvl)
        {
            healthMonster *= percentUpLvl;
            damageMonster *= percentUpLvl;
            lvlMonster += 1;
            damnLvl = false;
        }
    }

    private void IncreaseCaract(int numberOfMonster)
    {
        monster[numberOfMonster].maxHealth = healthMonster;
        monster[numberOfMonster].currentHealth = healthMonster;
        monster[numberOfMonster].damage = damageMonster;
        monster[numberOfMonster].lvl = lvlMonster;

        //Debug.Log("Actual monsters lvl : " + monster[numberOfMonster].lvl);

        //CHECK LEVEL AND CHANGE PREFAB GFX
        if (monster[numberOfMonster].lvl < 10)
        {
            if (monster[numberOfMonster].transform.GetChild(0) != null)
                monster[numberOfMonster].transform.GetChild(0).gameObject.SetActive(true);
            if (monster[numberOfMonster].transform.GetChild(1) != null)
                monster[numberOfMonster].transform.GetChild(1).gameObject.SetActive(false);
            if (monster[numberOfMonster].transform.GetChild(2) != null)
                monster[numberOfMonster].transform.GetChild(2).gameObject.SetActive(false);
        }
        if (monster[numberOfMonster].lvl >= 10)
        {
            if (monster[numberOfMonster].transform.GetChild(0) != null)
                monster[numberOfMonster].transform.GetChild(0).gameObject.SetActive(false);
            if (monster[numberOfMonster].transform.GetChild(1) != null)
                monster[numberOfMonster].transform.GetChild(1).gameObject.SetActive(true);
            if (monster[numberOfMonster].transform.GetChild(2) != null)
                monster[numberOfMonster].transform.GetChild(2).gameObject.SetActive(false);
        }
        if (monster[numberOfMonster].lvl >= 20)
        {
            if (monster[numberOfMonster].transform.GetChild(0) != null)
                monster[numberOfMonster].transform.GetChild(0).gameObject.SetActive(false);
            if (monster[numberOfMonster].transform.GetChild(1) != null)
                monster[numberOfMonster].transform.GetChild(1).gameObject.SetActive(false);
            if (monster[numberOfMonster].transform.GetChild(2) != null)
                monster[numberOfMonster].transform.GetChild(2).gameObject.SetActive(true);
        }
    }

    private void IncreaseMoney(int gToBuy, float percentG)
    {
        //INCREASE NUMBER OF GOLD TO UPGRADE OR BUY A MONSTER
        float moneyCh = gToBuy;
        moneyCh *= percentG;

        gToBuy = Mathf.RoundToInt(moneyCh);
    }

    private void InstantiateNewMonster()
    {
        GameObject newMonster = Instantiate(monsterPrefab, _monster[0].transform.position, Quaternion.identity) as GameObject;
        newMonster.transform.parent = this.gameObject.transform;

        newMonster.transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
        newMonster.transform.position = _monster[0].transform.position + new Vector3(Random.Range(-8, 8), 0, Random.Range(-8, 8));
    }

    #endregion

    #region AddClassMonster 

    private void AssignMonsterToLine()
    {
        //CHECK INDEX OF LIST
        foreach (GameObject mo in _monster)
        {
            index = _monster.IndexOf(mo);
            //Debug.Log("Index Monster : " + index);
        }
    }

    public void CreateLineMonsterClass()
    {
        //INSTANTIATE A PREFAB OF IMAGE CONTAINER
        GameObject newLine = Instantiate(linePrefab, lineList[0].transform.position, Quaternion.identity) as GameObject;
        newLine.transform.parent = lineContainer.transform;
        newLine.transform.localScale = Vector3.one;
        newLine.transform.position -= new Vector3(0, 100 * lineList.Count, 0);
    }
    #endregion
}
