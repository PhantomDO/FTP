﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingManager : MonoBehaviour {

    #region Variables

    [HideInInspector] public List<GameObject> _building = new List<GameObject>();

    [Header("Building")]
    public int numberOfBuildingMeet;
    public int numberOfBuildingDestroy;

    [Header("Object")]
    public GameObject prefabBuilding;
    public GameObject prefabMiniBoss;
    public GameObject prefabBoss;
    public GameObject prefabTerrain;

    [Header("Var")]
    public float movePosition;
    public int miniBossWave;
    public int bossWave;
    private Vector3 posBuilding;

    [Header("Leveling")]
    public float baseValueLvlUp;
    public float miniBossValueLvlUp;
    public float bossValueLvlUp;

    private float healthBuilding;
    private float goldBuilding;
    private float diamondBuilding;

    [Header("Percentage")]
    public int percentMaxValueDiamond;
    private int percentCritical;

    private MoneyManager manageM;

    #endregion

    #region Unity Methods

    private void Start()
    {
        manageM = FindObjectOfType<MoneyManager>();

        numberOfBuildingMeet = 1;

        foreach (GameObject j in _building)
        {
            healthBuilding = j.GetComponent<BuildingController>().stat._maxHealth;
            goldBuilding = j.GetComponent<BuildingController>().stat._gold;
        }
    }

    private void Update()
    {
        CheckBuilding();
    }

    #endregion

    #region Check

    private void CheckBuilding()
    {
        BuildingController[] building = GetComponentsInChildren<BuildingController>();

        for (int i = 0; i < building.Length; i++)
        {
            if (!_building.Contains(building[i].gameObject))
            {
                _building.Add(building[i].gameObject);
                posBuilding = building[i].gameObject.transform.position;
            }
        }
    }

    #endregion

    #region Respawn

    public void CreateAndRemove(GameObject buildingObj)
    {
        numberOfBuildingDestroy++;

        CheckLevels(buildingObj);
        DropGold(buildingObj);
        DropDiamond(buildingObj);

        _building.Remove(buildingObj);

        NewBuilding();

        prefabTerrain.transform.position += new Vector3(movePosition, 0, 0);
    }

    private void NewBuilding()
    {
        if (numberOfBuildingDestroy >= bossWave)
        {
            //Debug.Log("THE BOSS IS HERE");

            var myNewBuilding = Instantiate(prefabBoss, posBuilding + new Vector3(movePosition, 0, 0), Quaternion.identity);
            myNewBuilding.transform.parent = gameObject.transform;

            Leveling(bossValueLvlUp, myNewBuilding);

            numberOfBuildingMeet++;
            numberOfBuildingDestroy = 0;
        }
        else if (numberOfBuildingDestroy == miniBossWave)
        {
            //Debug.Log("THE MINI-BOSS IS HERE");

            var myNewBuilding = Instantiate(prefabMiniBoss, posBuilding + new Vector3(movePosition, 0, 0), Quaternion.identity);
            myNewBuilding.transform.parent = gameObject.transform;

            Leveling(miniBossValueLvlUp, myNewBuilding);

            numberOfBuildingMeet++;
        }
        else
        {
            //Debug.Log("NEW BUILDING");

            var myNewBuilding = Instantiate(prefabBuilding, posBuilding + new Vector3(movePosition, 0, 0), Quaternion.identity);
            myNewBuilding.transform.parent = gameObject.transform;

            Leveling(baseValueLvlUp, myNewBuilding);

            numberOfBuildingMeet++;
        }
    }

    #endregion

    #region Leveling

    private void CheckLevels(GameObject obj)
    {
        //Get Capacity
        healthBuilding = obj.GetComponent<BuildingController>().maxHealth;
        goldBuilding = obj.GetComponent<BuildingController>().gold;
        diamondBuilding = obj.GetComponent<BuildingController>().diamond;
    }

    private void Leveling(float percentage, GameObject newBuildings)
    {
        //Level Up
        healthBuilding *= percentage;
        goldBuilding *= percentage;
        diamondBuilding *= percentage;
        //Get the future building
        BuildingController bdControl = newBuildings.GetComponent<BuildingController>();
        //Apply new capacity
        bdControl.maxHealth = healthBuilding;
        bdControl.currentHealth = healthBuilding;
        bdControl.gold = Mathf.RoundToInt(goldBuilding);
        bdControl.diamond = Mathf.RoundToInt(diamondBuilding);
        //Debug.Log("Health Current : " + bdControl.currentHealth);
    }

    #endregion

    #region Drop

    private void DropDiamond(GameObject obj)
    {
        percentCritical = Random.Range(0, 100);

        if (percentCritical <= percentMaxValueDiamond)
        {
            manageM.EarnDiamond(obj.GetComponent<BuildingController>().diamond);
        }
        else
        {
            Debug.Log("No diamond found");
        }
    }

    private void DropGold(GameObject obj)
    {
        manageM.EarnGold(obj.GetComponent<BuildingController>().gold);
    }

    #endregion
}
