﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class UIManager : MonoBehaviour {

    #region Variables

    [Header("CountCurrency")]
    public Text countGold;
    public Text countDiamond;

    private MoneyManager findValueCurrency;
    private MonsterManager findValueCurrencyMonster;

    //___________________________________________________\\

    [Header("PriceMonster, DPS & Number")]
    public TextMeshProUGUI TMP_insertTextCostAdd;
    public TextMeshProUGUI TMP_InsertTextNumberG1;
    public TextMeshProUGUI TMP_InsertTextAddDps;

    public TextMeshProUGUI TMP_InsertTextCostUpgrade;
    public TextMeshProUGUI TMP_insertTextPowerDps;

    private TextContainer TMP_TextContainer;

    //___________________________________________________\\

    [Header("DailyRewardWindow")]
    public GameObject DailyRewardWindow;
    private bool activated;

    #endregion

    #region Unity Methods

    void Awake ()
    {
        findValueCurrency = FindObjectOfType<MoneyManager>();
        findValueCurrencyMonster = FindObjectOfType<MonsterManager>();
     
    }

    //___________________________________________________\\

    void Update ()
    {
        GoldCount();
        DiamondCount();

        MonsterInfoUpdate();
    }
    #endregion

    #region MonsterInfoUpdate
    void MonsterInfoUpdate()
    {
        TMP_insertTextCostAdd.text = findValueCurrencyMonster.gToBuyMonster.ToString();
        TMP_InsertTextNumberG1.text = findValueCurrencyMonster.numberOfMonster.ToString() + " / 20";
        TMP_InsertTextAddDps.text = "+" + findValueCurrencyMonster.DPStotal.ToString() ;

        TMP_InsertTextCostUpgrade.text = (findValueCurrencyMonster.gToBuyUpgrade 
                                        * findValueCurrencyMonster.numberOfMonster).ToString();
        TMP_insertTextPowerDps.text = "+" + findValueCurrencyMonster.DPStotal 
                                          * findValueCurrencyMonster.percentUpLvl 
                                          * findValueCurrencyMonster.numberOfMonster;
    }
    #endregion
    
    #region Currency counter

    void GoldCount()
    {
        countGold.text = findValueCurrency.numberOfGold.ToString();
    }

    void DiamondCount()
    {
        countDiamond.text = findValueCurrency.numberOfDiamond.ToString();
    }

#endregion

    #region DailyReward 


    public void OpenDailyReward()
    {
        if (!activated && !DailyRewardWindow.activeInHierarchy)
        {
            Debug.Log("Open");

            DailyRewardWindow.SetActive(true);
            activated = true;

            StartCoroutine(OpenReward());
        }
        if (!activated && DailyRewardWindow.activeInHierarchy)
        {
            Debug.Log("Close");

            DailyRewardWindow.SetActive(false);
            activated = true;

            StartCoroutine(OpenReward());
        }
    }

    private IEnumerator OpenReward()
    {
        yield return new WaitForSeconds(.1f);
        activated = false;
    }
#endregion

}
