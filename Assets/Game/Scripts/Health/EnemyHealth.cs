﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour, IDamageable {


    #region Variables
    [HideInInspector] public float currentHealth;
    [HideInInspector] public float maxHealth;

    private bool dead;
    #endregion

    #region Unity Methods
    protected virtual void Start()
    {
        currentHealth = maxHealth;
    }
    #endregion

    #region Damage
    public void TakeHit(float damage)
    {
        currentHealth -= damage;

        if (currentHealth <= 0 && !dead)
        {
            Die();
        }
    }
    #endregion

    #region Death
    protected void Die()
    {
        dead = true;
        GameObject.Destroy(gameObject);
    }
    #endregion
}
