﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MenuUISlider : MonoBehaviour
{
    #region Variables

    public Animator Anim;
    public bool isMoving;
    public bool onPressed;
    #endregion

    #region Unity Methods
    void Start()
    {
        isMoving = false;
    }
    #endregion

    #region Anim
    public void Press()
    {
        onPressed = true;

        if (!isMoving && onPressed)
        {
            Anim.SetBool("SliderOk", true);
            onPressed = false;
            isMoving = true;
        }

        if (isMoving && onPressed)
        {
            Anim.SetBool("SliderOk", false);
            onPressed = false;
            isMoving = false;
        }
    }
    #endregion
}