﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MarketScript : MonoBehaviour {

    #region Variables

    public GameObject MarketPlace;
    public GameObject buttonGameObject;

    private MoneyManager RemoveCurrency;

    [Header("SpecialItemValue")]

    public TextMeshProUGUI ChangeValueGold;
    public TextMeshProUGUI ChangeValueDiamonds;
    private int EggValueGold;
    private int EggValueDiamonds;



    #endregion

    #region Unity Methods

    public void Start()
    {
        RemoveCurrency = FindObjectOfType<MoneyManager>();

    }

    private void Update()
    {
       //EggValueGold = EggValueGold * ((PlaceInArray + 1) * 5);
       //EggValueDiamonds = EggValueDiamonds * (PlaceInArray + 1);
    }

    #endregion

    #region PlayAnimation

    public void Press()
    {
        MarketPlace.SetActive(true);
        buttonGameObject.GetComponent<Button>().interactable = false;
    }

    public void Close()
    {
        MarketPlace.SetActive(false);
        buttonGameObject.GetComponent<Button>().interactable = true;
    }

    #endregion

    #region MarketPrice

    #region OtherPack
    public void BuyPack100()
    {
        if (RemoveCurrency.numberOfDiamond >= 100)
            RemoveCurrency.numberOfDiamond = (RemoveCurrency.numberOfDiamond - 100);
    }

    public void BuyPack250()
    {
        if (RemoveCurrency.numberOfDiamond >= 250)
            RemoveCurrency.numberOfDiamond = (RemoveCurrency.numberOfDiamond - 250);
    }

    public void BuyPack500()
    {
        if (RemoveCurrency.numberOfDiamond >= 500)
            RemoveCurrency.numberOfDiamond = (RemoveCurrency.numberOfDiamond - 500);
    }

    public void BuyPack150()
    {
        if (RemoveCurrency.numberOfDiamond >= 150)
            RemoveCurrency.numberOfDiamond = (RemoveCurrency.numberOfDiamond - 150);
    }
    #endregion
    
    #region BuyEgg
    public void BuyEggWithDiamonds()
    {
        if (RemoveCurrency.numberOfDiamond >= EggValueDiamonds)
            RemoveCurrency.numberOfDiamond = RemoveCurrency.numberOfDiamond - EggValueDiamonds;
    }

    public void BuyEggWithGold()
    {
        if (RemoveCurrency.numberOfGold >= EggValueGold)
            RemoveCurrency.numberOfGold = RemoveCurrency.numberOfGold - EggValueGold;
    }

    void CurrencyUpdate()
    {
        ChangeValueDiamonds.text = EggValueDiamonds.ToString();
        ChangeValueGold.text = EggValueGold.ToString();
    }
    #endregion


    #endregion

}
